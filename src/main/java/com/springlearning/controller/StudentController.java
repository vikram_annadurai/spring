package com.springlearning.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springlearning.model.Student;
import com.springlearning.service.StudentService;

@RestController
@RequestMapping("/v1")
public class StudentController {

	@Autowired
	StudentService studentService;

	@PostMapping("/student/post")
	public Student create(@RequestBody Student data) {
		return studentService.create(data);
	}

	@GetMapping("/student/get")
	public List<Student> getall() {
		return studentService.getall();
	}
	
	@PutMapping("/student/update/{id}")
	public Student update(@PathVariable int id, @RequestBody Student data){
		return studentService.update(id, data);
	}

	@DeleteMapping("/student/delete/{id}")
	public List<Student> delete(@PathVariable int id) {
		return studentService.delete(id);
	}
	
	@DeleteMapping("student/deleteall")
	public String deleteall() {
		return studentService.deleteAll();
	}
}
