package com.springlearning.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.springlearning.model.Student;

@Component
public class StudentDao {

    List<Student> students = new ArrayList<>();
	
	public Student createStudent(Student data) {
		students.add(data);
		return data;
	}
	
	public List deleteStudent(int id) {
		System.out.println("blizz this is the id"+ id);
		for (Student i : students) {
			if (i.getId() == id) {
				students.remove(i);
			}
		}
		return students;
	}
	
	public List<Student> getAllStudent(){
		return students;
	}

	public String deleteAll() {
		boolean Status = students.removeAll(students);
		if (Status) {
			return "Deleted Successfully";
		} else {
			return "Something happend...not deleted";
		}
	}

	public Student updateStudent(int id, Student data) {
		for (Student i : students) {
			if (i.getId() == id) {
				i.setName(data.name);
				i.setAge(data.age);
				return i;
			}
		}
		return null;
	}
	
	
	
}
