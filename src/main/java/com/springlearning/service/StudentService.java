package com.springlearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.springlearning.dao.StudentDao;
import com.springlearning.model.Student;

//@Service
@Component
public class StudentService {

	@Autowired
	StudentDao studentDao;
	
	public Student create(Student data) {
		return studentDao.createStudent(data);
	}
	
	public List<Student> getall(){
		return studentDao.getAllStudent();
	}
	
	public List<Student> delete(int id) {
		return studentDao.deleteStudent(id);
	}
	
	public String deleteAll() {
		return studentDao.deleteAll();
	}

	public Student update(int id, Student data) {
		return studentDao.updateStudent(id, data);
	}
	
}
